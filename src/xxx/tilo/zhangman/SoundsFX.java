/**
 * Sound engine: fetches and plays appropriate sound effects
 * Uses Media Player which is not suited for long sounds and rapid switching
 */
package xxx.tilo.zhangman;

import java.io.IOException;
import java.util.Random;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

/**
 * @author Group 4 - Tilo, Daniel & Ermin
 * created: 26 Okt 2014
 * Education notes: We use two types of calls to Media Player to illustrate the two methods.
 * Here in SoundsFX we use MediaPlayer.setDataSource and in MainActivity we use
 * MediaPlayer.create().
 *
 */
public class SoundsFX {

	/**
	 * @return void
	 * argument Context CT is required to call getAssets() and should be the calling object; "this"
	 * argument sound is between 1-5
	 */
	public void playFX(Context CT, int sound) {
		Random srand = new Random();
		System.out.println("srand is:" + srand);

		final MediaPlayer mp = new MediaPlayer();

		if (mp.isPlaying()) {
			mp.stop();
			mp.reset();
			mp.release();
		}
		try {

			AssetFileDescriptor afd;

			switch (sound) {
			case 1: afd = CT.getAssets().openFd("button-29.mp3"); 			break;

			case 2: switch (srand.nextInt(4)) {
				case 0: afd = CT.getAssets().openFd("zombie_moan01.mp3"); 	break;
				case 1: afd = CT.getAssets().openFd("zombie_moan02.mp3"); 	break;
				case 2: afd = CT.getAssets().openFd("zombie_moan03.wav"); 	break;
				case 3: afd = CT.getAssets().openFd("zombie_moanf.mp3"); 	break;
				default:afd = CT.getAssets().openFd("zombie_moanf.mp3");} 	break;

			case 3:	 afd = CT.getAssets().openFd("Zombie_Brains.mp3"); 	  	break;
			case 4:	 afd = CT.getAssets().openFd("femalscream.wav");   		break;
			case 5:	 afd = CT.getAssets().openFd("zombie_come_here.mp3");   break;
			default: afd = CT.getAssets().openFd("button-29.mp3"); 	  		break;
			}

			mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
					afd.getLength());
			mp.prepare();
			mp.start();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
