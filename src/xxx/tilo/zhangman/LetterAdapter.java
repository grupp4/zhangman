package xxx.tilo.zhangman;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

/**
 * LetterAdapter creates our letter buttons that the user will press in the game.
 *  
 */
public class LetterAdapter extends BaseAdapter {

	// store letters
	private String[] letters;
	// inflater for button layout
	private LayoutInflater letterInf;
	/**
	 * Create our letters by first looping through char and then adding ÅÄÖ
	 * @param c is object GamePlay (this)
	 */
	public LetterAdapter(Context c) {
		// instantiate alphabet array
		letters = new String[29];
		for (int a = 0; a < letters.length; a++) {
			letters[a] = "" + (char) (a + 'A');
		}
		// Svenska bokstäver läggs till manuellt för att de är inte i ordning
		letters[26] = "Å";
		letters[27] = "Ä";
		letters[28] = "" + (char) 214; //Position i ascii-listan. Om keyb ej har åäö.

		// specify layout to inflate
		letterInf = LayoutInflater.from(c);
	}
	/**
	 * Dessa ärvas från abstrakt classen BaseAdapter 
	 * 
	 */
	@Override
	public int getCount() {
		return letters.length;
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// create a button for the letter at this position in the alphabet
		Button letterBtn;
		if (convertView == null) {
			// inflate the button layout
			letterBtn = (Button) letterInf.inflate(R.layout.letter, parent,
					false);
		} else {
			letterBtn = (Button) convertView;
		}
		// set the text to this letter
		letterBtn.setText(letters[position]);
		return letterBtn;
	}

}
