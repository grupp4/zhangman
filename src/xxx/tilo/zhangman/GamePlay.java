package xxx.tilo.zhangman;

import java.util.Random;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.GridView;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

/**
 * 
 * GamePlay handles the game logic of our ZHangman guess the word game.
 * 
 * Our secret word to be guessed is randomly taken from values/wordlistarr.xml which becomes 
 * currWord and each letter is set to invisible until correctly guessed.
 * Letter keys for the user to press are generated in LetterAdapter.java  on a GridView layout
 * with a loop in through char to generate each letter of the English alphabet, Swedish
 * letters added manually. 
 * bodyParts array stores our zombie to be hanged and set invisible until the player guesses
 * the wrong letter. Player can make numParts = 6 wrong guesses.
 * On the 7th wrong guess the zombie scares the player and dialog box pops up with a "you
 * lose" message. The box, which can also can be a win box, the player chooses to exit; return
 * to main or play again.
 * 
 * Sound effects in GamePlay.java are handled by SoundFX.java
 * Total losses and wins are saved with sharedpreferences and toast is used to send this message
 * 
 * @author Daniel, Tilo, Ermin 2014
 */
public class GamePlay extends ZactionBar {

	private int wonG = 0;
	private int lostG = 0;

	private String[] words;

	private String currWord;
	private LinearLayout wordLayout;
	private TextView[] charViews;
	private GridView letters;
	// body part images
	private ImageView[] bodyParts;
	// number of body parts
	private int numParts = 6; // Good to have since I use this in two places
	// current part - will increment when wrong answers are chosen
	private int currPart;
	// number of characters in current word
	private int numChars;
	// number correctly guessed
	private int numCorr;

	private SharedPreferences sh;
	private SoundsFX playSound = new SoundsFX();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_play);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		// read answer words in.
		// Vi gör en "resurshanterare" som sedan "hittar" xml-arrayen via R.java
		// och tar den arraylistan och sätter den till vår String-array
		Resources res = getResources();

		words = res.getStringArray(R.array.wordslistarr);

		// initialize word
		currWord = "";

		// get answer area
		// Vi hämtar den linjära layouten från xml'en som heter "word" med
		// findViewById
		// Vi måste också klargöra att det är en LinearLayout med att skriva
		// (LinearLayout)
		// Vår variabel wordLayout (som är typen LinearLayout) sätts till denna.
		wordLayout = (LinearLayout) findViewById(R.id.word_linear);

		// get letter button grid
		// På samma sätt hämtar vi XML'ens gridview som heter letters och sätter
		// den till vår java-variabel letters som är av typen GridView
		letters = (GridView) findViewById(R.id.letters_grid);

		// get body part images
		// bodyParts som är en array av typen ImageView skapas här
		// Vi lägger in varje hangmanbild i denna array.
		bodyParts = new ImageView[numParts];
		bodyParts[0] = (ImageView) findViewById(R.id.head_image);
		bodyParts[1] = (ImageView) findViewById(R.id.body_image);
		bodyParts[2] = (ImageView) findViewById(R.id.arm1_image);
		bodyParts[3] = (ImageView) findViewById(R.id.arm2_image);
		bodyParts[4] = (ImageView) findViewById(R.id.leg1_image);
		bodyParts[5] = (ImageView) findViewById(R.id.leg2_image);

		// start gameplay
		playGame();

	}

	@Override
	protected void onResume() {
		super.onResume();
		Toast.makeText(getApplicationContext(), "Welcome back!",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onStop() {
		super.onStop();
				sh = getSharedPreferences("xxx.tilo.zhangman.SETTINGS",
				Context.MODE_PRIVATE);
	String msg = "This session:\n " + "Wins: " + wonG + " Losses: " + lostG
				+ "\n\nTotal score:\nWins: " + sh.getInt("wonGame", 0)
				+ "Losses: " + sh.getInt("lostGame", 0);
		LayoutInflater inflater = getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast_layout,
				(ViewGroup) findViewById(R.id.toast_layout_root));
		TextView text = (TextView) layout.findViewById(R.id.text);
		text.setText(msg);
		Toast toast = new Toast(getApplicationContext());
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();

	}

	private void playGame() {

		// choose a word
		// Här tar vi ett ord från vår nya string-array. Vi gör en random
		// med arrayens storlek som maxvärde. Har arrayen bara 3 ord skall det
		// bli en random mellan 0-2.
		// initialize random
		Random rand = new Random();
		String newWord = words[rand.nextInt(words.length)];
		// make sure not same word as last time
		while (newWord.equals(currWord))
			newWord = words[rand.nextInt(words.length)];
		// update current word
		currWord = newWord;

		// create new array for character text views
		// En "vanlig" textView array som är så stor som currWord har bokstäver.
		charViews = new TextView[currWord.length()];

		// remove any existing letters
		wordLayout.removeAllViews();

		// loop through characters
		for (int c = 0; c < currWord.length(); c++) {
			// på plats c i arrayen initierar vi en TextView
			charViews[c] = new TextView(this);
			// set the current letter
			// på samma ställe sätter vi en text till TextView'n.
			// texten blir en bokstav på platsen "c"
			// Vi tar ordet på plats c från currWord och typomvandlar den från
			// char till String.
			charViews[c].setText("" + currWord.charAt(c));
			// set layout
			// Varje textView har massor av parametrar som layouter
			charViews[c].setLayoutParams(new LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			charViews[c].setGravity(Gravity.CENTER);
			charViews[c].setTextColor(Color.TRANSPARENT);
			charViews[c].setTextSize(22);
			charViews[c].setBackgroundResource(R.drawable.letter_bg);
			// add to display
			wordLayout.addView(charViews[c]);

		}

		// reset adapter
		LetterAdapter ltrAdapt = new LetterAdapter(this);
		letters.setAdapter(ltrAdapt);

		// start part at zero
		currPart = 0;
		// set word length and correct choices
		numChars = currWord.length();
		numCorr = 0;

		// hide all parts. Perhaps hiding the entire layout is better
		for (int p = 0; p < numParts; p++) {
			bodyParts[p].setVisibility(View.INVISIBLE);
		}
	}

	 
	/**
	 * letter pressed method, anropas från letter.xml; android:onClick="letterPressed"
	 * och är därför public
	 * @param view är button från LetterAdapter.java och vi då ändra olika states om
	 * den är nedtryckt eller inte
	 */	
	public void letterPressed(View view) {

		// find out which letter was pressed
		// View'n som sätts in i denna metod består av en enda bokstav.
		// Den får vi fram genom att göra en getText() från view'n och sen en
		// .toString().
		// Vi måste också skriva att det är en TextView genom att skriva
		// (TextView)
		String ltr = ((TextView) view).getText().toString();
		char letterChar = ltr.charAt(0);

		// disable view
		view.setEnabled(false);
		view.setBackgroundResource(R.drawable.letter_down);
		// check if correct
		boolean correct = false;
		for (int k = 0; k < currWord.length(); k++) {
			if (currWord.charAt(k) == letterChar) {
				correct = true;
				numCorr++;
				charViews[k].setTextColor(Color.WHITE);

			}
		}
		// check in case won
		if (correct) {
			playSound.playFX(this, 1);
			if (numCorr == numChars) {
				// disable all buttons
				disableBtns();

				// SharedPreferences keeps track on number of won games.
				sh = getSharedPreferences("xxx.tilo.zhangman.SETTINGS",
						Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sh.edit();
				editor.putInt("wonGame", (sh.getInt("wonGame", 0) + 1));
				editor.commit();
				wonG++;

				// let user know they have won, ask if they want to play again
				AlertDialog.Builder winBuild = new AlertDialog.Builder(this);
				winBuild.setTitle("YAY");
				winBuild.setMessage("You win!\n\nThe answer was:\n\n"
						+ currWord);
				winBuild.setPositiveButton("Play Again",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								GamePlay.this.playGame();
							}
						});
				winBuild.setNegativeButton("Exit",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								GamePlay.this.finish();
							}
						});
				winBuild.show();
			}
		}
		// check if user still has guesses
		else if (currPart < numParts) {
			// show next part
			bodyParts[currPart].setVisibility(View.VISIBLE);
			currPart++;
			// Player gussed wrong letter, but still has parts to keep playing!

			playSound.playFX(this, 2);
		}

		else {
			animate();

		}

	}

	// Will run only when the devices uses API 11 (honeycomb) or better
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void rotate(ImageView image, int degrees) {
		image.setRotation(degrees);
	}

	private void animate() {
		ImageView image = (ImageView) findViewById(R.id.head_image);
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.zombie_rush);
		// rotate(image,35); // requires API 11. and too slow, causing rendering
		// problems
		image.startAnimation(animation);
//		Drawable new_image= getResources().getDrawable(R.drawable.zhead5);   
//	    image.setImageDrawable(new_image);
//		image.startAnimation(animation);
//		Drawable new_image2= getResources().getDrawable(R.drawable.zhead3);   
//	    image.setImageDrawable(new_image2);
		playSound.playFX(this, 4);

		// Vibrate when lost.
		Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
		// Vibrate for 500 milliseconds
		v.vibrate(500);

		// hide everything except the head
		for (int a = 1; a < numParts; a++) {
			bodyParts[a].setVisibility(View.INVISIBLE);
		}

		// hide the views so head can pop out
		GridView foo = (GridView) findViewById(R.id.letters_grid);
		foo.setVisibility(4);
		LinearLayout bar = (LinearLayout) findViewById(R.id.word_linear);
		bar.setVisibility(4);
		// scare player for 3 secs then call lostGame();
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				lostGame();
			}

		}, 3000);

	}

	private void lostGame() {

		// user has lost

		disableBtns();
		// Use SharedPReferences to add number of lost games.
		sh = getSharedPreferences("xxx.tilo.zhangman.SETTINGS",
				Context.MODE_PRIVATE);

		SharedPreferences.Editor editor = sh.edit();
		editor.putInt("lostGame", (sh.getInt("lostGame", 0) + 1));
		editor.commit();
		lostG++;

		// let the user know they lost, ask if they want to play again
		AlertDialog.Builder loseBuild = new AlertDialog.Builder(this);
		loseBuild.setTitle("OOPS");
		loseBuild.setMessage("You lose!\n\nThe answer was:\n\n" + currWord);
		loseBuild.setPositiveButton("Play Again",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						GamePlay.this.playGame();

						// reset after zombie head stuff
						ImageView image = (ImageView) findViewById(R.id.head_image);
						// rotate(image,0);
						image.clearAnimation(); // This actually worked?! Got
												// rid of the big head
						GridView foo = (GridView) findViewById(R.id.letters_grid);
						foo.setVisibility(1);
						LinearLayout bar = (LinearLayout) findViewById(R.id.word_linear);
						bar.setVisibility(1);

					}
				});

		loseBuild.setNegativeButton("Exit",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						GamePlay.this.finish();
					}
				});
		loseBuild.show();

	}

	// disable letter buttons
	private void disableBtns() {
		int numLetters = letters.getChildCount();
		for (int l = 0; l < numLetters; l++) {
			letters.getChildAt(l).setEnabled(false);
		}
	}

}
