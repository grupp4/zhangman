package xxx.tilo.zhangman;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/**
 * 
 * @author Daniel, Tilo, Ermin 2014 This provides one ActionBar to all
 *         Activities using it.
 * 
 */
public class ZactionBar extends ActionBarActivity {
	private AlertDialog menuAlert;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		// int id = item.getItemId();
		// if (id == R.id.action_settings) {
		// return true;
		// }
		new SoundsFX().playFX(this, 1);
		switch (item.getItemId()) {

		case R.id.game_stat: {
			showGameStatistic();
			System.out.println(item.getItemId());
			return true;
		}
		case R.id.action_help: {
			showHelp();
			System.out.println(item.getItemId());
			return true;
		}
		/*
		case R.id.char_sheet: {
			showCharSheet();
			System.out.println(item.getItemId());
			return true;
		}
		*/
		case R.id.about_us: {
			showAboutUs();
			System.out.println(item.getItemId());
			return true;
		}
		}

		return super.onOptionsItemSelected(item);
	}

	private void showAboutUs() {
		Intent gameIntent = new Intent(this, AboutUs.class);
		gameIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(gameIntent);

	}

/*	private void showCharSheet() {
		Intent gameIntent = new Intent(this, CharSheet.class);
		startActivity(gameIntent);
		
	}
*/


	public void showGameStatistic() {
		SharedPreferences sh = getSharedPreferences(
				"xxx.tilo.zhangman.SETTINGS", Context.MODE_PRIVATE);

		AlertDialog.Builder statBuild = new AlertDialog.Builder(this);
		statBuild.setTitle("Game Stat");
		statBuild.setMessage("Number of won games: " + sh.getInt("wonGame", 0)
				+ "\n\nNumber of lost games: " + sh.getInt("lostGame", 0));
		statBuild.setPositiveButton("Gotcha!",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						menuAlert.dismiss();
					}
				});
		menuAlert = statBuild.create();
		statBuild.show();
	}

	// show help information
	private void showHelp() {
		AlertDialog.Builder helpBuild = new AlertDialog.Builder(this);
		helpBuild.setTitle("Help");
		helpBuild.setMessage("Guess the word by selecting the letters.\n\n"
				+ "You only have 6 wrong selections then it's game over!");
		helpBuild.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						menuAlert.dismiss();
					}
				});
		menuAlert = helpBuild.create();
		helpBuild.show();
	}

}
