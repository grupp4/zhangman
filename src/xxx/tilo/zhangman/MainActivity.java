package xxx.tilo.zhangman;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * 
 * @author Tilo,Daniel,Ermin 2014 10 28 Zhangman start activity with button
 *         calling GamePlay()
 * 
 */
public class MainActivity extends ZactionBar {

	// Adding media player
	MediaPlayer mp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button gameButton = (Button) findViewById(R.id.game_button);
		gameButton.setBackgroundResource(R.drawable.play_button);
		gameButton.setOnClickListener(buttonListener);

		Button muteButton = (Button) findViewById(R.id.ic_action_volume_on);
		muteButton.setBackgroundResource(R.drawable.ic_action_volume_on);
		muteButton.setOnClickListener(buttonListener);

		getSupportActionBar().setDisplayHomeAsUpEnabled(false);

		// Media Player - Check SoundsFX comments for more info about this
		// choice of player
		mp = MediaPlayer.create(MainActivity.this, R.raw.walking_dead);
		mp.start();

	}

	// Stopping music with home button
	@Override
	protected void onStop() {
		super.onStop();
		mp.stop();
	}

	private void whenClickedOn(View view) {
		new SoundsFX().playFX(this, 1);

		if (view.getId() == R.id.game_button) {
			Intent gameIntent = new Intent(this, GamePlay.class);
			startActivity(gameIntent);

		}
		if (view.getId() == R.id.ic_action_volume_on) {
			if (mp.isPlaying()) {
				mp.stop();
				view.setBackgroundResource(R.drawable.ic_action_volume_muted);
			} else {
				mp = MediaPlayer.create(this, R.raw.walking_dead);
				view.setBackgroundResource(R.drawable.ic_action_volume_on);
				mp.start();
			}
		}

	}

	private OnClickListener buttonListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			whenClickedOn(view); 

		}
	};

}
