package xxx.tilo.zhangman;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * 
 * @author Daniel, Ermin, Tilo 2014
 * A class about the authors
 */
public class AboutUs extends ZactionBar {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		Button backButton = (Button) findViewById(R.id.back_button);
		backButton.setOnClickListener(buttonListener);
	}

	private void whenClickedOn(View view) {
		if (view.getId() == R.id.back_button) {
			Intent mainIntent = new Intent(this, MainActivity.class);
			startActivity(mainIntent);
		}
	}

	private OnClickListener buttonListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			whenClickedOn(view);

		}
	};
}
